import React from 'react'
import { useDispatch } from 'react-redux'
import { baiTapDatveAction } from '../Store/slice'
import { useSelector } from 'react-redux'

const DanhSachHanGhe = ({hangGhe}) => {
  const dispatch = useDispatch()
  const {danhSachGheNgoi,danhSachGheDaDat} = useSelector(state => state.baiTapDatVeToolkit)
  let renderGheNgoi = () => {
    return hangGhe.danhSachGhe.map((ghe,index) => {
      let int = danhSachGheNgoi.findIndex(item => item.soGhe == ghe.soGhe )
      let int1 = danhSachGheDaDat.findIndex(item => item.soGhe == ghe.soGhe )
      let css = ''
      if (int!==-1) {
          css ='gheDangChon'
      }
      else if (int1!==-1) {
        css = 'gheDuocChon'
      }
      else {
        css = 'ghe'
      }
        return (
            <button onClick={() => dispatch(baiTapDatveAction.addGheNgoi(ghe))} key={index} className={ghe.daDat ? 'gheDuocChon' : css} style={{fontSize:'12px',marginLeft:'20px'}}>
                {ghe.soGhe}
            </button>
        )
      })
   }
   let renderHangGhe = () => {
    if(hangGhe.hang =='') {
        return (
            <div className='ml-3'>
               {hangGhe.danhSachGhe.map(ghe => {
                     return (
                        <button className='rowNumber'>
                        {ghe.soGhe}
                    </button>
                     )
                })} 

            </div>
        )
    }
    else {
        return ( <div>
            <p style={{display:'inline-block'}}>{hangGhe.hang}</p>{renderGheNgoi()}
          </div>)
    }
   }    
  return (
    <div className='ml-5 '>
     {renderHangGhe()}
    </div>
  )
}

export default DanhSachHanGhe