import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { baiTapDatveAction } from '../Store/slice'

const ThongTinDatGhe = () => {

   const {danhSachGheNgoi,danhSachGheDaDat} = useSelector(state => state.baiTapDatVeToolkit)
   console.log(danhSachGheDaDat)
   const dispatch = useDispatch()
   const  renderTable = ()=>{
      return danhSachGheNgoi.map((ghe) => {
         return (
            <tr >
               <td>{ghe.soGhe}</td>
               <td>{ghe.gia}</td>
               <td><button onClick={()=> dispatch(baiTapDatveAction.addGheNgoi(ghe))} className='btn btn-danger'>Hủy</button></td>
            </tr>
         )
      })           
   }
  return (
    <div className=''>
       <div style={{display:'flex' ,flexDirection:'column'}}>
        <div>
        <button className='gheDuocChon'></button><span className='text-light ml-2' style={{fontSize:'30px'}}>
        Ghế đã đặt
        </span>
        </div>
        <div>
        <button className='gheDangChon'></button><span className='text-light ml-2' style={{fontSize:'30px'}}>
        Ghế Đang Chọn
        </span>
        </div>
        <div>
        <button className='ghe m-0'></button><span className='text-light ml-2' style={{fontSize:'30px'}}>
        Ghế còn trống
        </span>
        </div>
       </div>
       <div className='mt-5'>
          <table className='table' border={2} style={{borderColor:'white'}} >
                <thead>
                 <tr className='text-light' style={{fontSize:'25px'}}>
                 <th>Số Ghế</th>
                    <th>Giá</th>
                    <th>Hủy</th>
                 </tr>
                </thead>
                <tbody className='text-light' style={{fontSize:'25px'}}>
                  {renderTable()}
                  <tr>
                     <td>Tổng Tiền</td>
                     <td>{danhSachGheNgoi.reduce((total,ghe) => total += ghe.gia,0)}</td>
                  </tr>
                </tbody>

          </table>
          <button className='btn btn-success' style={{fontSize:'25px'}} onClick={()=>dispatch(baiTapDatveAction.daDatGhe())}>Xác nhận đặt ghế</button>
       </div>
    </div>
  )
}

export default ThongTinDatGhe