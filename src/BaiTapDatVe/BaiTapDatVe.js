import React from "react";
import data from "../Data/danhSachGhe.json";
import DanhSachHanGhe from "./DanhSachHanGhe";
import "../Css/BaiTapBookingTicket.css";
import ThongTinDatGhe from "./ThongTinDatGhe";
import {useSelector} from 'react-redux'
const BaiTapDatVe = () => {
    const renderHanhGhe = () =>{
        return data.map((hangGhe,index) => {
            return (
                <DanhSachHanGhe hangGhe = {hangGhe} soHangGhe = {index}/>
            )
        })
    }
  return (
    <div
      className="bookingMovie"
      style={{
        position: "fixed",
        width: "100%",
        height: "100%",
        background: "url('./Img/bgmovie.jpg')",
        backgroundSize: "cover",
      }}
    >
      <div
        style={{
          backgroundColor: "rgba(0,0,0,0.5)",
          with: "100%",
          height: "100%",
        }}
      >
        <div className="container-fluid">
          <div className="row">
            <div className="col-8 col8">
              <div className="text-center display-4 text-warning">
                Bài Tập Đặt Vé
              </div>
              <div className="screen"></div>
            <div className="text-left text-light container" style={{fontSize:'25px'}} >
            {renderHanhGhe()}
            </div>
              
            </div>
            <div className="col-4">
              <div
                className="text-center display-4 text-light mt-5"
                style={{ fontSize: "50px" }}
              >
                Danh sách ghế bạn chọn
              </div>
              <ThongTinDatGhe />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BaiTapDatVe;
