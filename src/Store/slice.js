import { createSlice } from "@reduxjs/toolkit";
const initialState = {
   danhSachGheNgoi : [],
   danhSachGheDaDat : [],
}
const BaiTapDatVe  = createSlice({
    name : 'btDatve',
    initialState,
    reducers : {
        addGheNgoi : (state,action) => {
           let index = state.danhSachGheNgoi.findIndex(ghe => ghe.soGhe == action.payload.soGhe)
           if (index == -1) {
            state.danhSachGheNgoi.push(action.payload)
           } 
           else {
            state.danhSachGheNgoi.splice(index,1)
           }

        },
        daDatGhe  : (state,action) =>{
            state.danhSachGheNgoi.map(ghe => ghe.daDat = true)
            state.danhSachGheDaDat = [...state.danhSachGheDaDat,...state.danhSachGheNgoi]
            state.danhSachGheNgoi = []
        } 
    }
})
export const {actions : baiTapDatveAction , reducer : baiTapDatVeReducer} = BaiTapDatVe